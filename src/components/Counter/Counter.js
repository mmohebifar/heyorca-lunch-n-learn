import React, { Component } from 'react';
import { connect } from 'react-redux';

class Counter extends Component {
    handleClick(value) {
        this.props.dispatch({
            type: 'COUNTER',
            value
        });
    }

    render() {
        return (
            <div>
                {this.props.number}

                <button onClick={this.handleClick.bind(this, +1)}>+</button>
                <button onClick={this.handleClick.bind(this, -1)}>-</button>
            </div>
        );
    }
}

export default connect(
    function mapStateToProps(state) {
        return {
            number: state.number
        };
    }
)(Counter);
