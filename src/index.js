import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import Counter from './components/Counter/Counter';
import './index.css';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom';
import createStore from './redux/createStore';

const store = createStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/counter" component={Counter} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
