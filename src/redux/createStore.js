import { createStore } from 'redux';

const initialState = {
    number: 0
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case 'COUNTER':
            return {
                number: state.number + action.value
            };
        default:
            return state;
    }
}

export default function create() {
    return createStore(reducer);
}
